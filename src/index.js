#!/usr/bin/env node

const axios = require('axios')
const express = require('express')
var cors = require('cors')
const { createProxyMiddleware } = require('http-proxy-middleware');
const app = express()

app.use(cors())

const port = 4333
const target = "https://forms-dev.ikeaansokan.net/api/"
const fakeOrigin = "http://localhost:3000"

function onProxyReq(proxyReq, req, res) {
  proxyReq.setHeader('origin', fakeOrigin)
  console.log(`${req.method} ${req.url}`)
}

function onProxyRes(proxyRes, req, res) {
  proxyRes.headers['Access-Control-Allow-Origin'] = '*'
  console.log(res)
}

app.use('/*', createProxyMiddleware({ target, changeOrigin: true, onProxyReq, onProxyRes }));

app.listen(port, () => {
  console.log('Listening at port ' + port)
})